/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ntc.shapeproject;

/**
 *
 * @author L4ZY
 */
public class TestSquare {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) 
    {
        Square square1 = new Square(2);
        System.out.println("Area of square1 s = "+square1.getS()+" is "+ square1.calArea_Square());
        square1.setS(4);
        System.out.println("Area of square1 s = "+square1.getS()+" is "+ square1.calArea_Square());
        square1.setS(0);
        System.out.println("Area of square1 s = "+square1.getS()+" is "+ square1.calArea_Square());
        square1.setS(-2);
        System.out.println("Area of square1 s = "+square1.getS()+" is "+ square1.calArea_Square());
        square1.setS(6);
        System.out.println("Area of square1 s = "+square1.getS()+" is "+ square1.calArea_Square());
        
    }
    
}
