/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ntc.shapeproject;

/**
 *
 * @author L4ZY
 */
public class Triangle {
    private double h;
    private double b;
    public Triangle (double h,double b)
    {
        this.h =h;
        this.b =b;
    }
    public double calArea_Triangle()
    {
        return 0.5*h*b;
    }
    public double getH()
    {
        return h;
    }
    
    public double getB()
    {
        return b;
    }
    public void setH(double h){
        if(h<=0){
            System.out.println("ERROR height must more than zero!!!");
            return;
        } 
        this.h = h ;
    }
    public void setB(double b){
        if(b<=0){
            System.out.println("ERROR base must more than zero!!!");
            return;
        } 
        this.b = b ;
    }
    
    
}
