/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ntc.shapeproject;

/**
 *
 * @author L4ZY
 */
public class TestRectangle {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Rectangle rectangle1 = new Rectangle(4,5);
        System.out.println("Area of rectangle1 h = "+rectangle1.getH()+" and w = "+rectangle1.getW()+" is "+rectangle1.calArea_Rectangle());
        rectangle1.setH(4);
        rectangle1.setW(3);
        System.out.println("Area of rectangle1 h = "+rectangle1.getH()+" and w = "+rectangle1.getW()+" is "+rectangle1.calArea_Rectangle());
        rectangle1.setH(5);
        rectangle1.setW(0);
        System.out.println("Area of rectangle1 h = "+rectangle1.getH()+" and w = "+rectangle1.getW()+" is "+rectangle1.calArea_Rectangle());
        rectangle1.setH(6);
        rectangle1.setW(7);
        System.out.println("Area of rectangle1 h = "+rectangle1.getH()+" and w = "+rectangle1.getW()+" is "+rectangle1.calArea_Rectangle());
    }
    
}
